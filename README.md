# cataclock

Diseño y desarrollo de un reloj no convencional. Cataclock descuenta el tiempo a través de tres discos concéntricos que suplen la función de las manillas en los relojes tradicionales.

## Referencias:
 * [Información técnica del motor 28BYJ-48 - cookierobotics](https://cookierobotics.com/042/)
 * [Información técnica del motor 28BYJ-48 - lastminuteengineers](https://lastminuteengineers.com/28byj48-stepper-motor-arduino-tutorial/)
 * [Herramienta online para cálculo de engranajes - gear generator](https://geargenerator.com)
 * [Componente fritzing motor 28BYJ-48 - @tardate](https://github.com/tardate/X113647Stepper/blob/master/X113647Stepper.fzz)
 * [Extensión M-Block para motor 28BYJ-48 - @tgoorden](https://github.com/tgoorden/mblock-stepper)
 * [Identificación de las resistencias por color](https://resistorcolorcodecalc.com/)

## Licencias

Este trabajo esta sujeto a la licencia [GNU General Public v3.0 License](LICENSE-GPLV30). Todos los ficheros multimedia y de datos que no sean código fuente están sujetos a la licencia [Creative Commons Attribution 4.0 BY-SA license](LICENSE-CCBYSA40).

Más información acerca de estas licencias en [licencias Opensource](https://opensource.org/licenses/) y [licencias Creative Commons](https://creativecommons.org/licenses/).

